from influxdb import InfluxDBClient
import numpy as np


client = InfluxDBClient(database='testDb')
query = 'select phase, freq from phasor;'

print("Queying data: " + query)
rs = client.query(query)
p_points = list(rs.get_points(measurement='phasor'))

p_points = p_points[2:]

phases = [ x['phase'] for x in p_points ]
ideal_phases = [ 124.881817 - x*0.179006222 for x in range( len(phases) )]

diff = [ ideal_phases[i] - phases[i] for i in range(len(phases)) ]


freq = [ x['freq'] for x in p_points ] #59.95	

i=0
while i < len(diff):
	if diff[i] > 2:
		del diff[i]
		del freq[i]
	else:
		i += 1

delta_T = 0.2

#R:
# array([[ 0.05943322,  0.00325732],
#        [ 0.00325732,  0.02798737]])


# Q
# Q=[T^3/3, T^2/2; T^2/2, T]q