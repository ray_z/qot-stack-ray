#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/timex.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include <math.h>
#include <complex>


#include <linux/ptp_clock.h>

#include "bus/SPIDevice.h"
#include "gpio/GPIO.h"

#include "output_compare.hpp"

using namespace std;
using namespace exploringBB;

// Basic configurations
#define TIMELINE_UUID    "my_test_timeline"
#define APPLICATION_NAME "default"
#define OFFSET_MSEC      2000
#define DUTY_CYCLE       20
#define nSEC_PER_uSEC    1000
#define OC_MAGIC_NUM	 10601 //output compare time offset

//SPI configurations
#define SIG_FREQ 		 60
#define REPORT_RATE 	 2
#define REPORT_INTERVAL  nSEC_PER_SEC / REPORT_RATE
#define SAM_PER_CYC 	 17
#define SAM_PRD 		 ( nSEC_PER_SEC / SIG_FREQ + SAM_PER_CYC - 1) / SAM_PER_CYC // round up in ns
#define NUM_CYC			 3
#define SAM_COUNT 		 NUM_CYC * SAM_PER_CYC
#define HALF_SAM_TIME 	 SAM_PRD * (SAM_COUNT / 2) 
#define REF_VOLT 		 5.1
#define DELTA_T			 1.0 / (double) REPORT_RATE
#define BATCH_READ	     4

//PMU configurations
#define NUM_CHANNEL		 1
#define ALL_CHANL_BYTES  2 * NUM_CHANNEL
#define PHASE_OFFSET	 12.96 //deg 0.22619467105 rad -> 600 us lag from real power signal
#define AMP_SCALE_FACTOR 99.31468
#define qot_node_type    0
#define server_IP    	 "128.97.93.35"
#define server_port  	 "8086"


static int running = 1;

bool three_phase_system = FALSE;

static void exit_handler(int s)
{
   running = 0;
}

double bytes2voltage(unsigned char upper, unsigned char lower, bool bipolar_mode){
   int value = ( ( (int) upper ) << 8 )+ (int) lower;
   // cout << "value: " << value << endl;
   if(bipolar_mode){
      // cout << "bipolar\n";
      const int negative = (value & (1 << 11)) != 0;
      // cout << "negative:" << negative << endl;
      if (negative){
         value = value | ~((1 << 12) - 1); 
      }
      // cout << "value: " << value << endl;
   }
   return ( (double) value )/ 4096.0 * REF_VOLT;
}

qot_perout_t create_output_compare_request(struct timespec start, int period_nsec, int duty){
	qot_perout_t request;
    // Period: minimum period
    TL_FROM_nSEC(request.period, (int64_t) period_nsec);

    // Duty Cycle
    request.duty_cycle = duty;

    // Starting Edge -> Should be 1 for Rising, 2 for Falling
    request.edge = static_cast<qot_trigger_t>(2);
 
    request.start.sec = start.tv_sec;
    request.start.asec = start.tv_nsec * ASEC_PER_NSEC;  
    // printf("Start timestamp = %lld\n", (uint64_t) request.start.sec * nSEC_PER_SEC + request.start.asec / ASEC_PER_NSEC );

	return request;
}

uint64_t get_stop_ts(qot_perout_t request, struct timespec time_to_stop, int period_nsec){
    uint64_t stop_sec;
    uint64_t stop_nsec;
    stop_sec = request.start.sec + time_to_stop.tv_sec;
    stop_nsec = request.start.asec / ASEC_PER_NSEC + time_to_stop.tv_nsec;

    stop_sec = stop_nsec >= nSEC_PER_SEC ? stop_sec + 1 : stop_sec;
    stop_nsec = stop_nsec % nSEC_PER_SEC ;

    uint64_t stop_ts = stop_sec * nSEC_PER_SEC + stop_nsec - period_nsec; //rising edge of ptp pin should be less than stop ts
    // printf("stop timestamp = %lld\n", stop_ts);

    return stop_ts;
}

inline double Hamming_function(double n, double N){
	double N_1 = N-1;
	return 0.54 + 0.46 * cos( 2*M_PI*n / N_1 );
}

inline double rad2deg( double rad){
	return rad / M_PI * 180;
}

inline double round_up_mod(double a, double b){
	return a < 0 ? fmod( fmod(a, b) + b, b ) : fmod(a, b);
}

inline double map_angle_in_range(double angle ){//from -180 to 180
	if( angle > 180)
		return angle - 360;
	if( angle < -180 )
		return angle + 360;

	return angle;
}

//helper function for matrix computations
void matrix_copy(double A[][2], double B[][2]){ //A->B
	for (int i = 0; i < 2; ++i)
		for (int j = 0; j < 2; ++j)
			B[i][j] = A[i][j];
}

void matrix_add(double A[][2], double B[][2], double C[][2]){ //C=A+B
	double tmp[2][2] = { {0,0}, {0,0} };
	for (int i = 0; i < 2; ++i)
		for (int j = 0; j < 2; ++j)
			tmp[i][j] = A[i][j] + B[i][j];

	matrix_copy(tmp, C);
}

void matrix_sub(double A[][2], double B[][2], double C[][2]){ //C=A+B
	double tmp[2][2] = { {0,0}, {0,0} };
	for (int i = 0; i < 2; ++i)
		for (int j = 0; j < 2; ++j)
			tmp[i][j] = A[i][j] - B[i][j];

	matrix_copy(tmp, C);
}

void matrix_mult(double A[][2], double B[][2], double C[][2]){ //C = AB
	double tmp[2][2] = { {0,0}, {0,0} };
	for (int i = 0; i < 2; ++i)
		for (int j = 0; j < 2; ++j)
			for(int k = 0; k < 2; ++k)
				tmp[i][j] += A[i][k] * B[k][j];

	matrix_copy(tmp, C);
}

void inv_matrix(double A[][2], double C[][2]){ //assume 2D array
	double tmp[2][2] = { {0,0}, {0,0} };
	double det = A[0][0]*A[1][1] - A[0][1]*A[1][0];
	tmp[0][0] = A[1][1] / det;
	tmp[0][1] = -1*A[0][1] / det;
	tmp[1][0] = -1*A[1][0] / det;
	tmp[1][1] = A[0][0] / det;

	matrix_copy(tmp, C);
}

void trans_matrix(double A[][2], double C[][2]){ //assume 2D array
	double tmp[2][2] = { {0,0}, {0,0} };
	tmp[0][0] = A[0][0];
	tmp[0][1] = A[1][0];
	tmp[1][0] = A[0][1];
	tmp[1][1] = A[1][1];

	matrix_copy(tmp, C);
}

void print_matrix(double A[][2]){
	for (int i = 0; i < 2; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			cout << A[i][j] << " ";
		}
		cout << endl;
	}

}

int main(int argc, char *argv[]){
	printf("Program starts!\n");	
	cout << "SAM_PRD: " << SAM_PRD << endl;
	cout << "SAM_COUNT: " << SAM_COUNT << endl;
    cout << "HALF_SAM_TIME  = " <<  HALF_SAM_TIME << endl;

	signal(SIGINT, exit_handler);

	//SPI
	//===============================================================================
	int status;
	int num_Vol = 0;
	int offset = 0;
	int sam_acq = 0;
	//GPIO
	GPIO OE(117), EOC__(45); //CVNT__(14), P9_25: OE, P9_26: CVNT__, P8_11:EOC__
	// CVNT__.setDirection(OUTPUT);
	// CVNT__.setValue(HIGH);
	OE.setDirection(OUTPUT);
	OE.setValue(HIGH);
	EOC__.setDirection(INPUT);
	//SPI
	SPIDevice *busDevice = new SPIDevice(1,0); //Using second SPI bus (both loaded)
	busDevice->setSpeed(5000000);      // Have access to SPI Device object
	busDevice->setMode(SPIDevice::MODE0);
	unsigned char send[7], receive[7];
	unsigned char rcv_data[(SAM_COUNT + 2) * ALL_CHANL_BYTES];
	
	memset(send, 0, 7);
	memset(receive, 0, 7);
	memset(rcv_data, 0, (SAM_COUNT + 2) * ALL_CHANL_BYTES);


	//===============================================================================
	double voltage_values[SAM_COUNT];
	double voltage_values2[SAM_COUNT];
	double voltage_values3[SAM_COUNT];
	complex<double> i(0, 1);
	complex<double> N((double) SAM_COUNT, 0 );
	complex<double> pi(M_PI, 0);
	complex<double> exp_power = -i * 2.0 * pi * (double) NUM_CYC / N;
	complex<double> prev_phasor(0,0);
	complex<double> prev_phasor2(0,0);
	complex<double> prev_phasor3(0,0);
	complex<double> zero(0,0);
	double pre_del_freq = 0;
	double pre_phase_diff = 1;
	double ROCOF = 0;

	double Hamming_vals[SAM_COUNT];
	double Hamming_gain = 0;
	for (int j = - SAM_COUNT / 2; j <= SAM_COUNT / 2; ++j)
	{
		double tmp = Hamming_function( (double) j, (double) SAM_COUNT);
		Hamming_gain += tmp;
		Hamming_vals[j + SAM_COUNT / 2] = tmp;
	}
	cout << "Hamming_gain = " << Hamming_gain << endl;

	// ===============================================================================
	//Kalman filter stuff
	double Q[2][2] = { { pow(DELTA_T, 3)/2,  pow(DELTA_T, 2)/2 }, {pow(DELTA_T, 2)/2,0} };
	double R[2][2] = { {0.05943322, 0.00325732}, {0.00325732, 0.02798737} };
	double A[2][2] = { {1, DELTA_T * 2 *M_PI}, {0, 1} };
	double A_trans[2][2] = { {0,0}, {0,0} };
	trans_matrix(A, A_trans);
	double P_init[2][2] = { { pow(DELTA_T, 3)/2,  pow(DELTA_T, 2)/2 }, {pow(DELTA_T, 2)/2,0} };
	double P[2][2];
	matrix_copy(P_init, P);
	double B[2] = {M_PI * DELTA_T * DELTA_T, DELTA_T};
	double K_gain[2][2] = { {0,0}, {0,0} };
	double Identity[2][2] = { {1,0}, {0,1} };

	double arg_p_predict = 0;
	double freq_diff_predict = 0;
	cout << "R=" << endl;
	print_matrix(R);
	// ===============================================================================
	//HTTP post to PDC server	
	string server_addr = string("http://")+server_IP+":"+server_port+"/write?db=testDb";
	string phsr_msg = "";
	string msg_tmpl = "phasor,host=NESL,region=LA,node=pmu1,nominal_freq=60 "; //note the space at the end to seperate data from tag(influxDb)
	//===============================================================================
	//Output compare
	cout << "\nCreating timeline...\n";
	timeline_t *my_timeline;
    my_timeline = create_timeline();

	cout << "Timeline created\nWait for timeline to sync with master node..." << flush;
    sleep(15);
	cout << "Sync finished..\n";
    
    timepoint_t wake;
    get_current_time_from_timeline( my_timeline, &wake );

	struct timespec time_to_stop;
	time_to_stop.tv_sec = 0; // Stop ? seconds after the current System (GPS) Time
	time_to_stop.tv_nsec = nSEC_PER_SEC * NUM_CYC / SIG_FREQ;

	int period_nsec = SAM_PRD; // 1000000 microsecond = 1 second
	int duty = DUTY_CYCLE; // XX% duty cycle

	struct timespec start_ts[REPORT_RATE];
	struct timespec start;
	start.tv_sec = wake.sec + 1;
	start.tv_nsec = nSEC_PER_SEC - HALF_SAM_TIME - SAM_PRD / DUTY_CYCLE;// + OC_MAGIC_NUM ; 
	start_ts[0] = start;
	for (int j = 1; j < REPORT_RATE; ++j)
	{
		start.tv_sec = wake.sec + 2;
		start.tv_nsec =  j * REPORT_INTERVAL - HALF_SAM_TIME - SAM_PRD / DUTY_CYCLE;// + OC_MAGIC_NUM; 
		start_ts[j] = start;
	}

	// for (int j = 0; j < REPORT_RATE; ++j)
	// {
	// 	cout << "start time =" << start_ts[j].tv_sec << ":" << start_ts[j].tv_nsec << endl;
	// }

	cout << "PMU working in progress..." << endl;
	for (int k = 0; k < 15; ++k){
	// while(running){

		for (int p_idx = 0; p_idx < REPORT_RATE; p_idx++)
		{
			// cout << "\np_idx=" << p_idx << endl;
			start_ts[p_idx].tv_sec += 1;
			// cout << "start time =" << start_ts[p_idx].tv_sec << ":" << start_ts[p_idx].tv_nsec << endl;


			send[0] = 0x18; //lagging problem of output compare, need to clean buffer
			busDevice->transfer(send, receive, 1);
			send[0] = 0x4F; //0b01101111 for conver byte, 0b01001111 for pin
			send[1] = 0x04;  //channel 0 - 5
			status = busDevice->transfer(send, receive, 2);
			send[0] = 0xDE;  //conv bytes
			status = busDevice->transfer(send, receive, 1);
			send[0] = 0; send[1] = 0; send[2] = 0; send[3] = 0;
			// busDevice->transfer(send, receive, 2);
			// cout << "Response bytes are " << (int)receive[0] << "," << (int)receive[1]  << endl;
			
			qot_perout_t request;
			// cout << "start time =" << start_ts[p_idx].tv_sec << ":" << start_ts[p_idx].tv_nsec << endl;
			request = create_output_compare_request(start_ts[p_idx], period_nsec, duty);
			// cout << "start time =" << request.start.sec << ":" << request.start.asec / ASEC_PER_NSEC << endl;
			start_output_compare( my_timeline, request);

			//=======================================================================================
			struct timespec phasor_start_time;
			struct timespec phasor_stop_time;

		    qot_event_t output_event;
		    if(timeline_read_events(my_timeline, &output_event)) //Event 0
		    {
		        printf("Could not read output event 0\n");
		    }

		    //Loop till reach stop time
		    for (int sam_idx = 0; sam_idx < SAM_COUNT; ++sam_idx)
		    {
		        // timeline_read_events(my_timeline, &output_event); //falling edge
		        // printf("Falling Event detected at %lld %llu with event code %d\n", output_event.timestamp.estimate.sec, output_event.timestamp.estimate.asec / ASEC_PER_NSEC, output_event.type);
		        // timeline_read_events(my_timeline, &output_event); //rising edge
		        // printf("Rising Event detected at %lld %llu with event code %d\n", output_event.timestamp.estimate.sec, output_event.timestamp.estimate.asec / ASEC_PER_NSEC, output_event.type);

				int counter = 0;
				while(EOC__.getValue() != 0 && counter < 5000){ usleep(10); counter++; }  
				busDevice->transfer(send, rcv_data + sam_idx*ALL_CHANL_BYTES, ALL_CHANL_BYTES);

		    }
			stop_output_compare(my_timeline, request);

			// =====================================================================================
			// cout << "Raw data\n";
			double voltage;
			int corrupt_counter = 0;
			for (int j = 0; j < SAM_COUNT; ++j)
			{	
				// cout << "Response bytes are " << (int)rcv_data[j*2] << "," << (int)rcv_data[j*2+1]  << endl;
				if(  rcv_data[j*2] > 15 ){
					corrupt_counter++;
					voltage_values[j] = 0;
				}else{
					voltage_values[j]  = bytes2voltage(rcv_data[j*ALL_CHANL_BYTES], rcv_data[j*ALL_CHANL_BYTES+1], true) * Hamming_vals[ j ];
				}
				if (corrupt_counter >= 5)
				{
					cout << "\nCorrupted data -- 1\n"; 
					break;
				}
				// cout << voltage_values[j] << endl;
			}

			if (corrupt_counter < 5)
			{
				string tmp_msg = "";
				//calulate phasor
				complex<double> phasor(0, 0);
				for (int j = - SAM_COUNT / 2; j <= SAM_COUNT / 2; ++j)
				{
					if( voltage_values[j] > ( REF_VOLT * 1.2) ){
						corrupt_counter++;
					}
					if (corrupt_counter > 5)
					{
						cout << "\nCorrupted data -- 2\n"; 
						break;
					}

					phasor += voltage_values[ j + SAM_COUNT / 2 ] *  exp( exp_power * (double) j);
					
				}		
				phasor = phasor * sqrt(2.0) / Hamming_gain;				
				double mod_p = abs(phasor);
				double arg_p = arg(phasor);		
				double real_arg = map_angle_in_range( rad2deg( arg_p ) + PHASE_OFFSET );

				double arg_p_ph = arg(prev_phasor);
				double mod_p_ph = abs(prev_phasor);
				double phase_diff = 0;
				double freq_diff = 0;
				if (signbit(arg_p) != signbit(arg_p_ph) && abs(arg_p -arg_p_ph) > M_PI )
				{
					phase_diff = arg_p_ph > 0 ? arg_p + 2*M_PI - arg_p_ph : arg_p - 2*M_PI - arg_p_ph;
				}else{
					phase_diff = arg_p - arg_p_ph;
				}

				//check for corrputed/transient behaviours
				if( prev_phasor != zero && (
					// abs( round_up_mod( rad2deg( arg(phasor) ) - rad2deg(arg(prev_phasor)), 180 ) )  > 90
					abs( mod_p_ph - mod_p ) > 0.1 * mod_p_ph ||
					abs( phase_diff ) > abs( pre_phase_diff ) * 5
					)  ){

					cout << "|phasor| = " << mod_p * AMP_SCALE_FACTOR << endl;
					cout << "<phasor = " << real_arg  << " deg" << endl; 
					cout << "original <phasor = " << rad2deg( arg_p ) << " deg" << endl;  

					cout << "Corrupted Phasor (Possibly due to FIFO problem) \n"; 

					//re-initialize all variables
					phasor = zero; prev_phasor = zero; prev_phasor2 = zero; prev_phasor3 = zero;
					pre_del_freq = 0;
					pre_phase_diff = 1;
					ROCOF = 0;
					matrix_copy(P_init, P);
				}else{
					// cout << "Phasor = " << phasor << endl;
					// cout << "|phasor| = " << mod_p * AMP_SCALE_FACTOR << endl;
					// cout << "<phasor = " << real_arg << " deg" << endl; 
					// cout << "original <phasor = " << rad2deg( arg_p ) << " deg" << endl;  

					tmp_msg +=  msg_tmpl + "mag="+to_string(mod_p * AMP_SCALE_FACTOR)+",phase="+to_string(real_arg);
				
					if (prev_phasor != zero)
					{
						// cout << "phase_diff =" << phase_diff << endl;
						freq_diff = phase_diff / (DELTA_T * 2.0 * M_PI);
						tmp_msg += ",freq="+to_string( (double) SIG_FREQ + freq_diff );
						// cout << "freq_diff = " << freq_diff << endl;
						// cout << "freq= " << (double) SIG_FREQ + freq_diff << " Hz" << endl;
						pre_phase_diff = phase_diff;

						if ( pre_del_freq != 0 )
						{
							ROCOF = (freq_diff - pre_del_freq) / DELTA_T;
							// cout << "ROCOF=" << ROCOF << endl;

							//Kalman fitering
							//1. Project the prediction
							arg_p_predict = arg_p_ph + pre_del_freq * A[0][1] + ROCOF * B[0];
							freq_diff_predict = pre_del_freq + B[1] * ROCOF;

							//2. Project covariance
							double P_predict[2][2] = { {0,0}, {0,0} };
							matrix_mult(P, A_trans, P_predict);
							matrix_mult(A, P_predict, P_predict); 
							// matrix_add(P_predict, Q, P_predict);

							//3. Kalman gain
							matrix_add(P_predict, R, K_gain);
							inv_matrix( K_gain, K_gain );
							matrix_mult( P_predict, K_gain, K_gain );

							//4. Esitmate X
							double arg_mes_pred = arg_p - arg_p_predict;
							double freq_diff_mes_pred = freq_diff - freq_diff_predict;
							arg_p = arg_p_predict + K_gain[0][0] * arg_mes_pred + K_gain[0][1] * freq_diff_mes_pred;
							freq_diff = freq_diff_predict + K_gain[1][0] * arg_mes_pred + K_gain[1][1] * freq_diff_mes_pred;

							//5. Estimat P
							matrix_sub(Identity, K_gain, K_gain);
							matrix_mult(K_gain, P_predict, P);

							//6. Assign to Phasor
							phasor = polar( mod_p, arg_p );
							// cout << "\nKalman Filter adjusted value..." << endl;
							// cout << "|phasor| = " << mod_p * AMP_SCALE_FACTOR << endl;
							// cout << "<phasor = " << map_angle_in_range( rad2deg( arg_p ) + PHASE_OFFSET )  << " deg" << endl; 
							// cout << "freq_diff = " << freq_diff << endl;
							// cout << "freq= " << (double) SIG_FREQ + freq_diff << " Hz" << endl;
							tmp_msg = msg_tmpl + "mag="+to_string(mod_p * AMP_SCALE_FACTOR)+",phase="+to_string(real_arg)\
									+ ",freq="+to_string( (double) SIG_FREQ + freq_diff )\
									+ ",ROCOF="+to_string(ROCOF);

						}
						pre_del_freq = freq_diff;
					}
					prev_phasor = phasor; 
					uint64_t ts =  start_ts[p_idx].tv_sec * nSEC_PER_SEC + start_ts[p_idx].tv_nsec + HALF_SAM_TIME + SAM_PRD / DUTY_CYCLE;					
					phsr_msg += tmp_msg + " " + to_string( ts ) +"\n";
				}
			}
							
		}

		//upload to server
		cout << "Message = " << phsr_msg << endl;
		if( !phsr_msg.empty() ){
			string cmd = "curl -s -i -XPOST '"+ server_addr + "' --data-binary '" + phsr_msg + "' > /dev/null &"; //
			system( cmd.c_str() );
			phsr_msg = "";
		}

		//For 3 phase system
		if (three_phase_system)
		{
		    // cout << "Voltage 2\n";
			for (int j = 0; j < SAM_COUNT; ++j)
			{	
				// cout << "Response bytes are " << (int)rcv_data[j*ALL_CHANL_BYTES+2] << "," << (int)rcv_data[j*ALL_CHANL_BYTES+3]  << endl;
				voltage_values2[j] = bytes2voltage(rcv_data[j*ALL_CHANL_BYTES+2], rcv_data[j*ALL_CHANL_BYTES+3], true)* Hamming_vals[ j ];
				// cout << voltage << endl;
			}

			complex<double> phasor2(0, 0);
			for (int j = - SAM_COUNT / 2; j <= SAM_COUNT / 2; ++j)
			{
				phasor2 += voltage_values2[ j + SAM_COUNT / 2 ] *  exp( exp_power * (double) j);
				
			}		
			phasor2 = phasor * sqrt(2.0) / Hamming_gain;

		    // cout << "Voltage 3\n";
			for (int j = 0; j < SAM_COUNT; ++j)
			{	
				// cout << "Response bytes are " << (int)rcv_data[j*ALL_CHANL_BYTES+4] << "," << (int)rcv_data[j*ALL_CHANL_BYTES+5]  << endl;
				voltage_values[3j] = bytes2voltage(rcv_data[j*ALL_CHANL_BYTES+4], rcv_data[j*ALL_CHANL_BYTES+5], true)* Hamming_vals[ j ];
				// cout << voltage << endl;
			}
			
			complex<double> phasor2(0, 0);
			for (int j = - SAM_COUNT / 2; j <= SAM_COUNT / 2; ++j)
			{
				phasor3 += voltage_values3[ j + SAM_COUNT / 2 ] *  exp( exp_power * (double) j);
				
			}		
			phasor3 = phasor * sqrt(2.0) / Hamming_gain;

			complex<double> alpha = exp (-i * 2.0 * pi / 3.0);
			complex<double> postive_seq_phasor = phasor + alpha * phasor2 + alpha * alpha * phasor3;
			cout << "Postive sequence phasor = " << postive_seq_phasor << endl;
		}

	}

	//clean up resources
	cout << "\nPMU closing..." << endl;
	cout << "Destory Timeline\n";
	distory_timeline(my_timeline);

	printf("Program stops!\n");

}