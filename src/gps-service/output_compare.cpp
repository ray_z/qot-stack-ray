/*
 * @file qot.h
 * @brief Header file for the GPS module
 * @author Fatima Anwar
 *
 * Copyright (c) Regents of the University of California, 2015.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/timex.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

extern "C"
{
    #include <linux/ptp_clock.h>
}

#include "output_compare.hpp"

// Basic onfiguration
#define TIMELINE_UUID    "my_test_timeline"
#define APPLICATION_NAME "default"
#define OFFSET_MSEC      2000
#define DUTY_CYCLE       50
#define nSEC_PER_uSEC    1000


timeline_t* create_timeline(){
    timeline_t *my_timeline;
    timelength_t resolution;
    resolution.sec = 0; resolution.asec = 1e9; // 1nsec
    timeinterval_t accuracy;
    accuracy.below.sec = 0; accuracy.below.asec = 1e12; accuracy.above.sec = 0; accuracy.above.asec = 1e12; // 1usec

    const char *u = TIMELINE_UUID;
    const char *m = APPLICATION_NAME;    

    /** CREATE TIMELINE **/
    my_timeline = timeline_t_create();
    if(!my_timeline)
    {
        printf("Unable to create the timeline data structure\n");
        QOT_RETURN_TYPE_ERR;
    }

    // Bind to a timeline
    // printf("Binding to timeline %s ........\n", u);
    if(timeline_bind(my_timeline, u, m, resolution, accuracy))
    {
        printf("Failed to bind to timeline %s\n", u);
        timeline_t_destroy(my_timeline);
        // return QOT_RETURN_TYPE_ERR;
    }

    return my_timeline;
}

qot_return_t get_current_time_from_timeline( timeline_t* my_timeline, timepoint_t* wake){
    utimepoint_t wake_now;

    // Read Initial Time
    if(timeline_gettime(my_timeline, &wake_now))
    {
        printf("Could not read timeline reference time\n");
        // Unbind from timeline
        if(timeline_unbind(my_timeline))
        {
            printf("Failed to unbind from timeline \n");
            timeline_t_destroy(my_timeline);
            return QOT_RETURN_TYPE_ERR;
        }
        timeline_t_destroy(my_timeline);
        return QOT_RETURN_TYPE_ERR;
    }
    else
    {
        *wake = wake_now.estimate;
        //timepoint_add(&wake, &request.period);
        //wake.asec = 0;
    }    

    return QOT_RETURN_TYPE_OK;
}


/**
 * launch output compare functionality
 * start in absolute time and stop in duration
 **/
qot_return_t start_output_compare( timeline_t* my_timeline, qot_perout_t request){

    qot_callback_t callback;

    if(timeline_enable_output_compare(my_timeline, &request))
    {
        printf("Cannot request periodic output\n");
        // Unbind from timeline
    	if(timeline_unbind(my_timeline))
    	{
        	printf("Failed to unbind from timeline\n");
        	timeline_t_destroy(my_timeline);
        	return QOT_RETURN_TYPE_ERR;
    	}
    	printf("Unbound from timeline \n");

    	// Free the timeline data structure
    	timeline_t_destroy(my_timeline);
    	return QOT_RETURN_TYPE_ERR;
    }
    else
    {
        // printf("Output Compare Succesful\n");
    }
    
    /* Success */
    return QOT_RETURN_TYPE_OK;

}

/* Stop output compare */
qot_return_t stop_output_compare(timeline_t* my_timeline, qot_perout_t request){

    if(timeline_disable_output_compare(my_timeline, &request))
    {
        printf("Cannot disable periodic output\n");
        return QOT_RETURN_TYPE_ERR;
    }
    // printf("Output Compare disabled\n");
    /* Success */
    return QOT_RETURN_TYPE_OK;
}

/* Destory timeline */
qot_return_t distory_timeline( timeline_t* my_timeline ){
    /** DESTROY TIMELINE **/

    // Unbind from timeline
    if(timeline_unbind(my_timeline))
    {
        printf("Failed to unbind from timeline \n" );
        timeline_t_destroy(my_timeline);
        return QOT_RETURN_TYPE_ERR;
    }
    // printf("Unbound from timeline \n");

    // Free the timeline data structure
    timeline_t_destroy(my_timeline);
}
