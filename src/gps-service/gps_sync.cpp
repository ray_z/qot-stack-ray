/* Read timestamp from GPS. Reference: derek molley    */

#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/timex.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <iostream>

#include <termios.h> // for UART
#include <sys/select.h>

#include "output_compare.hpp"

extern "C"
{
  #include <linux/ptp_clock.h>// for ptp pin

  /* Add servo and clock adjustment code from LinuxPTP */
  #include "linuxptp/servo.h"
  #include "linuxptp/clockadj.h"
}

/* Add servo and clock adjustment code from LinuxPTP -- INSTALL LINUXPTP */
// #include "config.h"
// #include "servo.h"
// #include "clockadj.h"

/* Useful definitions */
#define NSEC_PER_SEC  ((int64_t)1000000000)
#define NSEC_PER_MSEC ((int64_t)1000000)
#define FD_TO_CLOCKID(fd) ((~(clockid_t) (fd) << 3) | 3)


#ifndef CLOCK_INVALID
#define CLOCK_INVALID -1
#define GPS_SYNC_INTERVAL 10

#endif


#define qot_node_type      1

// #define PTP_PIN_SETFUNC    _IOW(PTP_CLK_MAGIC, 7, struct ptp_pin_desc)
// struct ptp_pin_desc {
//     /*
//      * Hardware specific human readable pin name. This field is
//      * set by the kernel during the PTP_PIN_GETFUNC ioctl and is
//      * ignored for the PTP_PIN_SETFUNC ioctl.
//      */
//     char name[64];
//     /*
//      * Pin index in the range of zero to ptp_clock_caps.n_pins - 1.
//      */
//     unsigned int index;
    
//      * Which of the PTP_PF_xxx functions to use on this pin.
     
//     unsigned int func;
//     /*
//      * The specific channel to use for this function.
//      * This corresponds to the 'index' field of the
//      * PTP_EXTTS_REQUEST and PTP_PEROUT_REQUEST ioctls.
//      */
//     unsigned int chan;
//     /*
//      * Reserved for future use.
//      */
//     unsigned int rsv[5];
// };

using namespace std;


int buffer_clear = 6;

static int running = 1;

static void exit_handler(int s)
{
   printf("Exit requested \n");
   running = 0;
}

int main(int argc, char *argv[]){
  // Set up uart and ptp
  printf("Program starts!\n");

  // cout << "Create timeline..." << endl;
  // timeline_t *my_timeline;
  // my_timeline = create_timeline();

  // cout << "Create qotdaemon..\n";
  // string cmd = "qotdaemon -m " + to_string( qot_node_type ) + " &";
  // cout << "cmd = " << cmd << endl;
  // system( cmd.c_str() );
  // sleep(15);

  cout << "Entering loop..\n";

  printf("Set up uart4 ...\n");
  int uart_fd, count;
  if ((uart_fd = open("/dev/ttyO4", O_RDWR | O_NOCTTY))<0){ //  | O_NDELAY
    perror("UART: Failed to open the file.\n");
    return -1;
  }
  struct termios options;
  tcgetattr(uart_fd, &options);
  options.c_cflag = B9600 | CS8 | CREAD | CLOCAL;
  options.c_iflag = IGNPAR | ICRNL;
  options.c_lflag = ~ICANON;
  options.c_cc[VMIN] = 35;
  options.c_cc[VTIME] = 0;
  tcflush(uart_fd, TCIFLUSH);
  if ( tcsetattr(uart_fd, TCSANOW, &options) < 0)
  {  
    printf("Cannot set tcsetattr\n");
  }

  printf("Set uart4 success\n");

  const char *device_m = "/dev/ptp1";               /* PTP device */
  int index_m = 1;                            /* Channel index, '1' corresponds to 'TIMER6' */
  int fd_m;                                   /* device file descriptor */
  clockid_t clkid;

  int c, cnt;
  struct ptp_clock_caps caps;                 /* Clock capabilities */
  struct ptp_pin_desc desc;                   /* Pin configuration */
  struct ptp_extts_event event;               /* PTP event */
  struct ptp_extts_request extts_request;     /* External timestamp req */

  /* How we plan to discipline the clock */
  printf("Plan to discipline the clock ...\n");
  int max_ppb;
  double ppb, weight;
  uint64_t local_ts;
  int64_t offset;
  struct servo *servo;

/***********************************************************/
  //read ts from GPS at arriving pps signal
  printf("Set GPS output to NMEA_SEN_ZDA...\n");
  //Set GPS NEMA output format
  char gps_cmd[] = "$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0*29\r\n";
  unsigned char receive[200];
  memset(receive, 0, 200);

  // if ( ( count = write(uart_fd, &gps_cmd, strlen(gps_cmd) ) ) <0 ){
  //    perror("Failed to write to the output\n");
  //    return -1;
  // }

/***********************************************************/
  printf("Set up ptp pin ...\n");
  /* Open the character device */
  fd_m = open(device_m, O_RDWR);
  if (fd_m < 0) {
    fprintf(stderr, "opening device %s: %s\n", device_m, strerror(errno));
    return -1;
  }
  printf("Device opened %d\n", fd_m);

  struct termios ptp_options;
  tcgetattr(fd_m, &ptp_options);
  ptp_options.c_lflag = ~ICANON;
  ptp_options.c_cc[VTIME] = 10;
  tcflush(fd_m, TCIFLUSH);
  if ( tcsetattr(fd_m, TCSANOW, &ptp_options) < 0)
  {  
    printf("Cannot set tcsetattr for ptp pin\n");
  }

  printf("Set ptp options success\n");


  memset(&desc, 0, sizeof(desc));
  desc.index = index_m;
  desc.func = 1;              // '1' corresponds to external timestamp
  desc.chan = index_m;
  if (ioctl(fd_m, PTP_PIN_SETFUNC, &desc)) {
    printf("Set pin func failed for %d\n", fd_m);
    return -1;
  }
  printf("Set pin func succees for %d\n", fd_m);

  // Request timestamps from the pin
  memset(&extts_request, 0, sizeof(extts_request));
  extts_request.index = index_m;
  extts_request.flags = PTP_ENABLE_FEATURE | PTP_RISING_EDGE;
  if (ioctl(fd_m, PTP_EXTTS_REQUEST, &extts_request)) {
    printf("Requesting timestamps failed for %d\n", fd_m);
    return -1;
  }
  printf("Requesting timestamps success for %d\n", fd_m);
/******* Setup Linuxptp for processor clock adjustment *******/

    /* Get Clock ID for the processor clock */
  clkid = FD_TO_CLOCKID(fd_m);
  if (CLOCK_INVALID == clkid) {
    printf("error: failed to read clock id\n");
    return -1;
  }
  printf("Clkid successfully got as %d\n", clkid);
	
  printf("Setup Linuxptp for processor clock adjustment...\n");
	/* Determine slave max frequency adjustment */
	if (ioctl(fd_m, PTP_CLOCK_GETCAPS, &caps)) {
		printf("error: cannot get capabilities\n");
	}
	max_ppb = caps.max_adj;
	if (!max_ppb) {
		printf("error: clock is not adjustable\n");
	}
	
	/* Initialize clock discipline */
	clockadj_init(clkid);
	
	/* Get the current ppb error */
	ppb = clockadj_get_freq(clkid);
	
	/* Create a servo */
	servo = servo_create(
						 //cfg,			/* Servo configuration */
						 CLOCK_SERVO_PI,	/* Servo type */
						 -ppb,			/* Current frequency adjustment */
						 max_ppb, 		/* Max frequency adjustment */
						 0				/* 0: hardware, 1: software */
						 );
	
	/* Set the servo sync interval (in fractional seconds) */
	servo_sync_interval(
						servo,
						GPS_SYNC_INTERVAL
            //ptp_clock_double(&perout_request.period)
						);
	
	/* This will save the servo state */
  enum servo_state state;
  double dmax, dmin;

/***********************************************************/
   sleep(2);

   printf("Clear PTP input capture buffer...\n");
   fd_set set;
   struct timeval timeout;
   int read_status;

   FD_ZERO(&set);
   while( buffer_clear > 0 ){//input cature events saved in the ptp pin buffer, need to clear them
      /* Read events coming in */
      FD_SET(fd_m, &set);

      timeout.tv_sec = 1;
      timeout.tv_usec = 0;

      read_status = select(fd_m + 1, &set, NULL, NULL, &timeout);
      if( read_status == -1)
              cout << "Error reading from ptp..." << endl;
      else if (read_status == 0)
              cout << "Time out reading from ptp..." << endl;
      else{
              cnt = read(fd_m, &event, sizeof(event));
              if (cnt != sizeof(event)) {
                      cout << "Cannot read event" << endl;
                      break;
              }
              printf("Core Time - %lld.%09u\n", event.t.sec, event.t.nsec);
              buffer_clear--;
      }

   }
   cout << "Ptp input capture buffer cleared\n";

   printf("Clear GPS output...\n");
   tcflush(uart_fd, TCIOFLUSH);

   int counter = 0;

	 printf("Adjustment begins...\n");
   signal(SIGINT, exit_handler);

   //for ptp pin read timeout
   do{
      printf("\nTrying to read events %d\n", running++);
      usleep(200000); //wait for GPS output
      cnt = read(uart_fd, (void*)receive, 35);
      if (cnt <0){
        perror("Failed to read from the input\n");
        return -1;
      }else if (cnt==0) 
        printf("There was no data available to read!\n\n");
      else {
         // printf("The following was read in [%d]: \n%.35s\n", cnt ,receive);

        char hr[3];  hr[0]=receive[7]; hr[1]=receive[8]; hr[2] = '\0';
        char min[3]; min[0]=receive[9]; min[1]=receive[10]; min[2] = '\0';
        char sec[3]; sec[0]=receive[11]; sec[1]=receive[12]; sec[2] = '\0';
        char msec[4]; msec[0]=receive[14]; msec[1]=receive[15]; msec[2]=receive[16];  msec[3] = '\0';
        char day[3]; day[0]=receive[18]; day[1]=receive[19]; day[2] = '\0';
        char mon[3]; mon[0]=receive[21]; mon[1]=receive[22]; mon[2] = '\0';
        char yr[5];  yr[0]=receive[24]; yr[1]=receive[25]; yr[2]=receive[26]; yr[3]=receive[27]; yr[4] = '\0';
        // printf("hr:%s, min:%s, sec:%s, d:%s, m:%s, y:%s  \n", hr, min, sec, day, mon, yr);

        struct tm tm; time_t time;
        tm.tm_sec = atoi(sec);
        int ms = atoi( msec );
        tm.tm_min = atoi(min);
        tm.tm_hour = atoi(hr);
        tm.tm_mday = atoi(day);
        tm.tm_mon = atoi(mon) - 1;
        tm.tm_year = atoi(yr) - 1900;


        // printf("%d %d %d %d %d %d  \n", tm.tm_sec, tm.tm_min, tm.tm_hour, tm.tm_mday, tm.tm_mon, tm.tm_year);


        time = mktime(&tm);
        printf("gps ts(in nsec):%lld\n", (uint64_t) time * NSEC_PER_SEC + (uint64_t)  ms * NSEC_PER_MSEC);

        /* Read events coming in */
        FD_SET(fd_m, &set);

        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        read_status = select(fd_m + 1, &set, NULL, NULL, &timeout);
        if( read_status == -1)
                cout << "Error reading from ptp..." << endl;
        else if (read_status == 0)
                cout << "Time out reading from ptp..." << endl;
        else{
                cnt = read(fd_m, &event, sizeof(event));
                if (cnt != sizeof(event)) {
                        cout << "Cannot read event" << endl;
                        break;
                }
                printf("Core Time - %lld.%09u\n", event.t.sec, event.t.nsec);


                uint64_t ptp_ts = event.t.sec * NSEC_PER_SEC +  event.t.nsec;
                printf("ptp ts(in nsec):%lld\n", ptp_ts);
                // uint64_t gps_ts = (uint64_t) time * NSEC_PER_SEC;
                // printf("ptp:%llu  gps:%llu    Offset: %lld  \n", ptp_ts, gps_ts, (int64_t) ptp_ts - gps_ts);

                int64_t tmp = ((int64_t) event.t.sec -(int64_t) time) * NSEC_PER_SEC;
                // printf("tmp: %lld\n", tmp);
                tmp += (int64_t) event.t.nsec - (int64_t)  ms * NSEC_PER_MSEC;
                printf("Offset: %lld\n", tmp);

         

                if(counter == 0){
                  /*** TIME SYNCHRONIZATION: ALIGN PROCESSOR CLOCK TO GPS CLOCK ***/
                  /* Local timestamp and offset */
                  local_ts = ptp_ts;
                  offset = tmp;
                  weight = 1.0;
                
                  /* Update the clock */
                  cout << "Update the clock...\n";
                  // ppb = servo_sample(
                  //          servo,       /* Servo object */
                  //          offset,      /* T(slave) - T(master) */
                  //          local_ts,    /* T(master) */
                  //          //weight,      /* Weighting */
                  //          &state       /* Next state */
                  //          );

                  ppb = servo_sample(
                      CLOCK_SERVO_PI,
                      servo,      /* Servo object */
                      offset,     /* T(slave) - T(master) */
                      local_ts,     /* T(master) */
                      //weight,     /* Weighting */
                      &state,       /* Next state */
                      &dmax,
                      &dmin
                    );
                  printf("ppb:%lf\n", ppb);
                   // What we do depends on the servo state 
                  switch (state) {
                    case SERVO_UNLOCKED:
                      cout << "SERVO_UNLOCKED...\n";
                      break;
                    case SERVO_JUMP:
                      cout << "SERVO_JUMP...\n";
                      clockadj_step(clkid, -offset);
                    case SERVO_LOCKED:
                      cout << "SERVO_LOCKED... \n";
                      clockadj_set_freq(clkid, -ppb);
                      break;
                  }
                
                }
                 
                counter++;
                if (counter >= GPS_SYNC_INTERVAL)
                {
                  counter = 0;
                }


        }

      }

   }while (running);


  //clean up resources
  cout << "Close uart port...\n";
  close(uart_fd);
	
	/* Destroy the servo */
	cout << "Destroy servo...\n";
  servo_destroy(servo);

  /* Disable the pin */
  cout << "Disable ptp input capture pin..." << endl;
  memset(&desc, 0, sizeof(desc));
  desc.index = index_m;
  desc.func = 0;              // '0' corresponds to no function
  desc.chan = index_m;
  if (ioctl(fd_m, PTP_PIN_SETFUNC, &desc)) {
    printf("Disable pin func failed for %d\n", fd_m);
  }

  /* Close the character device */
  close(fd_m);


  // cout << "\nDestory Timeline\n";
  // distory_timeline(my_timeline);

  printf("Program finishes!\n");
  return 0;
}
